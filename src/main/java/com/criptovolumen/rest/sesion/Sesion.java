package com.criptovolumen.rest.sesion;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "SESION")
public class Sesion {

	@Id
	private String user_id;
	
	private String session_id;

	private Instant last_connection;
	
	private String ip;
	
	public Sesion(String user_id, String session_id, String ip) {
		this.user_id = user_id;
		this.session_id = session_id;
		this.last_connection = Instant.now();
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "Sesion [user_id=" + user_id + ", session_id=" + session_id + ", last_connection=" + last_connection + ", ip=" + ip + "]";
	}
	
	
}
