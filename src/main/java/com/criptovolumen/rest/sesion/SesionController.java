package com.criptovolumen.rest.sesion;

import java.time.Instant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Scope(value = "singleton")
@RestController
public class SesionController {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SesionRepository sesionRepository;
	
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/sesion", method = RequestMethod.POST)
	public ResponseEntity<Object> getVelas(HttpServletRequest request, HttpServletResponse response,//
			@FormParam("user_id") String user_id,//
			@FormParam("session_id") String session_id,//
			@FormParam("ip") String ip){
		if (StringUtils.isBlank(user_id) || StringUtils.isBlank(session_id) || StringUtils.isBlank(ip)) {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
		Sesion sesion = new Sesion(user_id, session_id, ip);
		sesionRepository.save(sesion);
		log.info("Registrado {}", sesion.toString());
		response.setStatus(HttpStatus.ACCEPTED.value());
		return ResponseEntity.accepted().build();
	}
}
