package com.criptovolumen.rest.sesion;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SesionRepository  extends CrudRepository<Sesion, String> {

	@Query(nativeQuery = true, value = "select * from SESION s WHERE s.user_id = ?1 AND s.ip = ?2 AND s.session_id = ?3 ")
	public List<Sesion> buscarSesiones(String usuario, String ip, String sesion);

}
