package com.criptovolumen.rest.vela;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MarkDTO {

	private List<Long> id;
	private List<Long> time;
	private List<String> color;
	private List<String> text;
	private List<String> label;
	private List<String> labelFontColor;
	private List<Long> minSize;
	
	public MarkDTO() {
		this.id = new ArrayList<>();
		this.time= new ArrayList<>();
		this.color = new ArrayList<>();
		this.text = new ArrayList<>();
		this.label = new ArrayList<>();
		this.labelFontColor = new ArrayList<>();
		this.minSize= new ArrayList<>();
	}
}
