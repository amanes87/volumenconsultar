package com.criptovolumen.rest.vela;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.criptovolumen.entidades.vela.Vela;
import com.criptovolumen.entidades.vela.par.VelaBTC_15;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_120Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_15Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_240Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_30Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_60Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_720Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_DRepository;
import com.criptovolumen.rest.sesion.AESCBC;
import com.criptovolumen.rest.sesion.Sesion;
import com.criptovolumen.rest.sesion.SesionRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Scope(value = "singleton")
@RestController
public class VelaController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private VelaBTC_15Repository velaBTC_15Repository;

	@Autowired
	private SesionRepository sesionRepository;
	
	@Autowired
	private VelaBTC_30Repository velaBTC_30Repository;
	
	@Autowired
	private VelaBTC_60Repository velaBTC_60Repository;
	
	@Autowired
	private VelaBTC_120Repository velaBTC_120Repository;
	
	@Autowired
	private VelaBTC_240Repository velaBTC_240Repository;
	
	@Autowired
	private VelaBTC_720Repository velaBTC_720Repository;
	
	@Autowired
	private VelaBTC_DRepository velaBTC_DRepository;

//	@CrossOrigin(origins = "http://35.169.119.46:9090")
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public ResponseEntity<String> getVelas(HttpServletRequest request, //
			@RequestParam(value="token", required=false) String token,//
			@RequestParam(value="symbol", required=false) String symbol,
			@RequestParam(value="resolution", required=false) String resolution,
			@RequestParam(value="from", required=false) long from,
			@RequestParam(value="to", required=false) long to,
			HttpServletResponse response) {
		if(tokenValido(token, request.getRemoteAddr())) {
			Iterable<? extends Vela> velas = obtenerVelas(resolution, from, to);
			return respuestaVelaComun(velas);
		}else {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
	}
	
	private Iterable<? extends Vela> obtenerVelas(String resolution, long from, long to) {
		Iterable<? extends Vela> velas = null;
		if(resolution.equals("15")) {
			velas = velaBTC_15Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("30")) {
			velas = velaBTC_30Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("60")) {
			velas = velaBTC_60Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("120")) {
			velas = velaBTC_120Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("240")) {
			velas = velaBTC_240Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("720")) {
			velas = velaBTC_720Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}else if(resolution.equals("1D")) {
			velas = velaBTC_DRepository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
		}
	return velas;
}

	private ResponseEntity<String> respuestaVelaComun(Iterable<? extends Vela> velas){
		VelaDTO velaDTO = obtenerVelaDTO(velas);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(velaDTO);
		velas.forEach(v -> {
		});
		return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS)).eTag(velaDTO.hashCode() + "").body(json);
	}
		
	private VelaDTO obtenerVelaDTO(Iterable<? extends Vela> velas) {
		VelaDTO velaDTO = new VelaDTO();
		velas.forEach(v -> {
			velaDTO.getT().add(v.getTime_period_start().toEpochMilli() / 1000);
			velaDTO.getC().add(v.getPrice_close());
			velaDTO.getO().add(v.getPrice_open());
			velaDTO.getH().add(v.getPrice_high());
			velaDTO.getL().add(v.getPrice_low());
			velaDTO.getV().add((long) v.getVolume_traded());
			if (Double.valueOf(v.getPrice_close()).equals(new Double(7197.4d))) {
				log.error("VELA DTO!!!!! {}", v.toString());
			}
		});
		if(CollectionUtils.isEmpty(velaDTO.getT())) {
			velaDTO.setS("no_data");
		}
		return velaDTO;
	}
	
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/marks", method = RequestMethod.GET)
	public ResponseEntity<String> getMarks(HttpServletRequest request, //
			@RequestParam(value = "token", required = false) String token, //
			@RequestParam(value = "symbol", required = false) String symbol,
			@RequestParam(value = "resolution", required = false) String resolution,
			@RequestParam(value = "from", required = false) long from,
			@RequestParam(value = "to", required = false) long to, HttpServletResponse response) {
		/*
		//		{
//		    id: [array of ids],
//		    time: [array of times],
//		    color: [array of colors],
//		    text: [array of texts],
//		    label: [array of labels],
//		    labelFontColor: [array of label font colors],
//		    minSize: [array of minSizes],
//		}
		
		
		/*
		 * {
    "id": [
        0,
        1,
        2,
        3,
        4,
        5
    ],
    "time": [
        1543190400,
        1542844800,
        1542585600,
        1542585600,
        1541894400,
        1540598400
    ],
    "color": [
        "red",
        "blue",
        "green",
        "red",
        "blue",
        "green"
    ],
    "text": [
        "Today",
        "4 days back",
        "7 days back + Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "7 days back once again",
        "15 days back",
        "30 days back"
    ],
    "label": [
        "A",
        "B",
        "CORE",
        "D",
        "EURO",
        "F"
    ],
    "labelFontColor": [
        "white",
        "white",
        "red",
        "#FFFFFF",
        "white",
        "#000"
    ],
    "minSize": [
        14,
        28,
        7,
        40,
        7,
        14
    ]
		 */

		if (tokenValido(token, request.getRemoteAddr())) {
			if (resolution.equals("15")) {
//				List<VelaBTC_15> listaVelas = velaBTC_15Repository.findVelasBetweenDatesSortedAsc(Instant.ofEpochSecond(from), Instant.ofEpochSecond(to));
//				MarkDTO markDTO = new MarkDTO();
//				List<Vela> listaVelasRellenar = new ArrayList<>();
//				for (int i = 0; i < listaVelas.size(); i++) {
//					Vela vela = listaVelas.get(i);
//					listaVelasRellenar.add(listaVelas.get(i));
//					if (listaVelasRellenar.size() > 30) {
//						Double mediaVolumen = calcularMediaVolumen30(listaVelasRellenar);
//						List <Double> volumenesAnteriores3 = new ArrayList<>();
//						volumenesAnteriores3.add(listaVelas.get(i - 3).getVolume_traded());
//						volumenesAnteriores3.add(listaVelas.get(i - 2).getVolume_traded());
//						volumenesAnteriores3.add(listaVelas.get(i - 1).getVolume_traded());
//						int superadosMedia = 0;
//						for(Double volumenAnterior : volumenesAnteriores3) {
//							if(volumenAnterior.compareTo(mediaVolumen) > 0) {
//								superadosMedia ++;
//							}
//						}
//						Double precioCierre3Anterior = listaVelas.get(i - 3).getPrice_close();
//						Double precioCierreActual = listaVelas.get(i).getPrice_close();
//						Double precioCierreAnteriorMultiplicado = precioCierre3Anterior * 1.025;
//						
//						boolean esBearish = false;
//						if (precioCierreActual.compareTo(precioCierreAnteriorMultiplicado) > 0 && superadosMedia > 1) {
//							//precio sube y volumen sube - Bullish
//							esBearish = false;
//						}
//						if (precioCierreActual.compareTo(precioCierreAnteriorMultiplicado) > 0 && superadosMedia <= 1) {
//							//precio sube y volumen baja - Bullish
//							esBearish = false;
//						}
//						Double precioCierreAnteriorMultiplicadoNegativo = precioCierre3Anterior * 0.975;
//						if (precioCierreActual.compareTo(precioCierreAnteriorMultiplicadoNegativo) < 0 && superadosMedia > 1) {
//							//precio baja y volumen sube - Bearish
//							esBearish = true;
//						}
//						if (precioCierreActual.compareTo(precioCierreAnteriorMultiplicadoNegativo) < 0 && superadosMedia <= 1) {
//							//precio baja y volumen baja - Bearish
//							esBearish = true;
//						}
//						
//						if(esBearish) {
//							markDTO.getColor().add("red");
//							
//						}else {
//							markDTO.getColor().add("green");
//						}
//						Long tiempoEnSegundos = vela.getTime_period_start().toEpochMilli() / 1000;
//						markDTO.getId().add(tiempoEnSegundos);
//						markDTO.getTime().add(tiempoEnSegundos);
//						markDTO.getMinSize().add(4L);
//					}
//				}
			// velas.forEach(v -> {
			// velaDTO.getT().add(v.getTime_period_start().toEpochMilli() / 1000);
			// velaDTO.getC().add(v.getPrice_close());
			// velaDTO.getO().add(v.getPrice_open());
			// velaDTO.getH().add(v.getPrice_high());
			// velaDTO.getL().add(v.getPrice_low());
			// velaDTO.getV().add((long) v.getVolume_traded());
			// });
//			Gson gson = new GsonBuilder().setPrettyPrinting().create();
//			String json = gson.toJson(markDTO);
//			return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS))
//					.eTag(markDTO.hashCode() + "").body(json);
		} else if (resolution.equals("30")) {
		} else if (resolution.equals("60")) {
		} else if (resolution.equals("120")) {
		} else if (resolution.equals("240")) {
		} else if (resolution.equals("720")) {
		} else if (resolution.equals("D")) {
		}
	}else{
		response.setStatus(HttpStatus.FORBIDDEN.value());
		return ResponseEntity.noContent().build();
	}
		return null;
	}

	private Double calcularMediaVolumen30(List<Vela> listaVelas) {
		Double media = 0d;
		if (listaVelas != null && !listaVelas.isEmpty() && listaVelas.size() > 30) {
			Comparator<Vela> velaPorTiempoCierreDescendente = (Vela v1, Vela v2) -> v2.getTime_close().compareTo(v1.getTime_close());
			List<Vela> velasAnteriores = listaVelas.stream().sorted(velaPorTiempoCierreDescendente).skip(1).limit(30).collect(Collectors.toList());
			double acumulador = 0d;
			for (Vela vela : velasAnteriores) {
				acumulador = acumulador + vela.getVolume_traded();
			}
			 media = acumulador / 30;
		}
		return media;
	}

//	@CrossOrigin(origins = "http://35.169.119.46:9090")
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/symbols", method = RequestMethod.GET)
	public ResponseEntity<String> getSymbols(HttpServletRequest request, @RequestParam(value="token", required=false) String token, HttpServletResponse response) {
		if(tokenValido(token, request.getRemoteAddr())) {
//			String respuesta = "{\"name\":\"BTCUSDT\",\"exchange-traded\":\"BITFINEX\",\"exchange-listed\":\"BITFINEX\",\"timezone\":\"America/New_York\",\"minmov\":1,\"minmov2\":0,\"pricescale\":10,\"pointvalue\":1,\"session\":\"0930-1630\",\"has_intraday\":true,\"has_no_volume\":false,\"ticker\":\"BBOYS\",\"description\":\"Crypto de la buena\",\"type\":\"stock\",\"supported_resolutions\":[\"15\",\"30\",\"60\",\"120\",\"240\",\"720\",\"D\",\"2D\",\"3D\",\"W\",\"3W\",\"M\"],\"base_name\":[\"BTCUSDT\"],\"legs\":[\"BTCUSDT\"],\"exchange\":\"BITFINEX\",\"full_name\":\"BITFINEX:BTCUSDT\",\"data_status\":\"streaming\"}";, \"expired\":true, \"expiration_time\":1532454683
			String respuesta = "{\"name\":\"BTCUSD\",\"exchange-traded\":\"BITFINEX\",\"exchange-listed\":\"BITFINEX\",\"timezone\":\"America/New_York\",\"minmov\":1,\"minmov2\":0,\"pricescale\":10,\"pointvalue\":1,\"session\":\"24x7\",\"has_intraday\":true,\"has_daily\":true,\"has_no_volume\":false,\"ticker\":\"BTCUSD\",\"description\":\"BTCUSD Consolidated\",\"type\":\"stock\",\"supported_resolutions\":[\"15\",\"30\",\"60\",\"120\",\"240\",\"720\",\"D\"],\"base_name\":[\"BTCUSD\"],\"legs\":[\"BTCUSD\"],\"exchange\":\"BITFINEX\",\"full_name\":\"BITFINEX:BTCUSD\",\"data_status\":\"streaming\"}";
			return ResponseEntity.ok().body(respuesta);
		}else {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
	}

//	@CrossOrigin(origins = "http://35.169.119.46:9090")
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/config", method = RequestMethod.GET)
	public ResponseEntity<String> getConfig(HttpServletRequest request, @RequestParam(value="token", required=false) String token, HttpServletResponse response) {
		log.info("TOKEN {}", token);
		if(tokenValido(token, request.getRemoteAddr())) {
			log.info("Usuario Validado!");
//			String respuesta = "{\"supports_search\":true,\"supports_group_request\":false,\"supports_marks\":false,\"supports_timescale_marks\":true,\"supports_time\":false,\"exchanges\":[{\"value\":\"\",\"name\":\"All Exchanges\",\"desc\":\"\"},{\"value\":\"XETRA\",\"name\":\"XETRA\",\"desc\":\"XETRA\"},{\"value\":\"NSE\",\"name\":\"NSE\",\"desc\":\"NSE\"},{\"value\":\"NasdaqNM\",\"name\":\"NasdaqNM\",\"desc\":\"NasdaqNM\"},{\"value\":\"NYSE\",\"name\":\"NYSE\",\"desc\":\"NYSE\"},{\"value\":\"CDNX\",\"name\":\"CDNX\",\"desc\":\"CDNX\"},{\"value\":\"Stuttgart\",\"name\":\"Stuttgart\",\"desc\":\"Stuttgart\"}],\"supported_resolutions\":[\"15\",\"30\",\"60\",\"120\",\"240\",\"720\",\"D\"],\"symbols_types\":[{\"name\":\"All types\",\"value\":\"\"},{\"name\":\"Stock\",\"value\":\"stock\"},{\"name\":\"Index\",\"value\":\"index\"}]}";
			String respuesta = "{\"supports_search\":true,\"supports_group_request\":false,\"supports_marks\":true,\"supports_timescale_marks\":false,\"supports_time\":false,\"supported_resolutions\":[\"15\",\"30\",\"60\",\"120\",\"240\",\"720\",\"D\"]}";
			return ResponseEntity.ok().body(respuesta);
		}else {
			log.info("TOKEN NO VALIDO");
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
		
	}
	
//	@CrossOrigin(origins = "http://35.169.119.46:9090")
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/time", method = RequestMethod.GET)
	public ResponseEntity<String> getTime(HttpServletRequest request, @RequestParam(value="token", required=false) String token, HttpServletResponse response) {
		if(tokenValido(token, request.getRemoteAddr())) {
			return ResponseEntity.ok().body("1542026700");
		}else {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
	}
	
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/symbol_info", method = RequestMethod.GET)
	public ResponseEntity<String> getSymbolInfo(HttpServletRequest request,@RequestParam(value="token", required=false) String token, @RequestParam(value="group", required=false) String group, HttpServletResponse response){
		String respuesta = "{ symbol: [\"AAPL\", \"MSFT\", \"SPX\"], description: [\"Apple Inc\", \"Microsoft corp\", \"S&P 500 index\"], exchange-listed: \"NYSE\", exchange-traded: \"NYSE\", minmovement: 1, minmovement2: 0, pricescale: [1, 1, 100], has-dwm: true, has-intraday: true, has-no-volume: [false, false, true] type: [\"stock\", \"stock\", \"index\"], ticker: [\"AAPL~0\", \"MSFT~0\", \"$SPX500\"], timezone: “America/New_York”, session-regular: “0900-1600”, }";
		if(tokenValido(token, request.getRemoteAddr())) {
			return ResponseEntity.ok().body(respuesta);
		}else {
			response.setStatus(HttpStatus.FORBIDDEN.value());
			return ResponseEntity.noContent().build();
		}
	}
	
	private boolean tokenValido(String token, String ipRemota) {
//		log.info("USUARIO IP REMOTA: {}", ipRemota);
		boolean tokenEsValido = false;
		String tokenDesencriptado;
		try {
			tokenDesencriptado = AESCBC.desencriptar(token, "1234567890123456", "1234567890123456");
			log.info("TOKEN DESENCRIPTADO {}", tokenDesencriptado);
			String[] tokenArray = tokenDesencriptado.split("\\|");
			if (tokenArray != null && tokenArray.length > 2) {
				String usuario = tokenArray[0];
				String sesion = tokenArray[1];
				log.info("USUARIO {}", usuario);
				log.info("IP REMOTA {}", ipRemota);
//				log.info("SESION {}", sesion);
				List<Sesion> sesiones = sesionRepository.buscarSesiones(usuario, ipRemota, sesion);
				log.info("HAY SESIONES? {}",!CollectionUtils.isEmpty(sesiones));
				tokenEsValido = !CollectionUtils.isEmpty(sesiones);
			}
		} catch (InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
				| NoSuchPaddingException e) {
			log.error("Error validando token {}", e.getMessage());
		}
		return tokenEsValido;
//		return true;
	}
	
	@CrossOrigin(origins = "https://www.volumechart.com")
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<String> getMarks(HttpServletRequest request){
		return ResponseEntity.ok().body("TIENE CONEXION");
	}
}
