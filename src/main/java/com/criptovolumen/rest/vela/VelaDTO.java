package com.criptovolumen.rest.vela;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VelaDTO {

	private String s;
	
	private List<Long> t;
	private List<Double> c;
	private List<Double> o;
	private List<Double> h;
	private List<Double> l;
	private List<Long> v;

	public VelaDTO() {
		this.s = "ok";
		this.t = new ArrayList<>();
		this.c = new ArrayList<>();
		this.o = new ArrayList<>();
		this.h = new ArrayList<>();
		this.l = new ArrayList<>();
		this.v = new ArrayList<>();
	}
	
}
