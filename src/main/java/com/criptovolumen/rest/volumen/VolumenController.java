package com.criptovolumen.rest.volumen;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.criptovolumen.entidades.volumen.par.VolumenBTC;
import com.criptovolumen.entidades.volumen.servicios.VolumenBTCRepository;

@Scope(value = "singleton")
@RestController
public class VolumenController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	// @Autowired
	// private UserDetailsService uds;

	@Autowired
	private VolumenBTCRepository volumenBTCRepository;

	@RequestMapping(value = "/volumen/{moneda}", method = RequestMethod.GET)
	public ResponseEntity<Iterable<VolumenBTC>> getVolumen(//
			@PathVariable("moneda") String moneda,// 
			@RequestHeader("If-None-Match") String etag,//
			HttpServletResponse response) {
		log.info("GET VOLUMEN {}", moneda);
		Iterable<VolumenBTC> volumenes = volumenBTCRepository.findAllSimpleVolumenBTC();
		if (etag.equals(volumenes.hashCode() + "")) {
			response.setStatus(HttpStatus.NOT_MODIFIED.value());
			return null;
		} else {
//			List<VelaDTO> volumenesSimple = StreamSupport.stream(volumenes.spliterator(), false).map(vbtc -> new VelaDTO(vbtc)).collect(Collectors.toList());
			return ResponseEntity.ok().cacheControl(CacheControl.maxAge(30, TimeUnit.DAYS)).eTag(volumenes.hashCode() + "")
					.body(volumenes); // lastModified
		}
	}
	
}
