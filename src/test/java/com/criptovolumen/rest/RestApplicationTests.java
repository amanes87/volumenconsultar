package com.criptovolumen.rest;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.criptovolumen.rest.sesion.AESCBC;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void desencriptarToken() {
		String token = "oUFNtfVJYyrqZV9hRjNtIQz1Zcx8ecRbd1qFFZUkwHmdUoqkmkzFYTfu0p9rzwvbTC1saTuXWwL%2FRPDZRApc0eoFfPJVJprfQSm%2BwWyi188%3D";
		try {
			System.out.println("DESENCRIPTAR: " + AESCBC.desencriptar(token, "1234567890123456", "1234567890123456"));
		} catch (InvalidKeyException | UnsupportedEncodingException | InvalidAlgorithmParameterException
				| IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
				| NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
